import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OneComponent } from './Components/one/one.component';


const routes: Routes = [
  {
    path: 'one',
    component: OneComponent
  },
  { path: "one/:page", component: OneComponent },
  { path: "", redirectTo: "one", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
