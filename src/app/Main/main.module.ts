import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { OneComponent } from './Components/one/one.component';


@NgModule({
  declarations: [OneComponent],
  imports: [
    CommonModule,
    MainRoutingModule
  ]
})
export class MainModule { }
