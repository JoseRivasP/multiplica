import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../Services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss'],
})
export class OneComponent implements OnInit {
  public page;
  public per_page;
  public total;
  public total_pages;
  public data;
  public ad;
  public next_page;
  public prev_page;

  constructor(
    private _dataService: DataService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.actualPage();
  }

  actualPage() {
    this._route.params.subscribe((params) => {
      let page = +params['page'];
      this.page = page;

      if (!params['page']) {
        page = 1;
      }

      if (!page) {
        page = 1;
      } else {
        this.next_page = page + 1;
        this.prev_page = page - 1;

        if (this.prev_page <= 0) {
          this.prev_page = 1;
        }

        if (this.next_page > this.total_pages) {
          this.next_page = 1;
        }

        if (page > this.total_pages) {
          this.next_page = 1;
        }

        if (this.next_page > this.total_pages) {
          this.next_page = 1;
        }
        if (params['page'] == this.total_pages) {
          this.next_page = 1;
        }
        if (!params['page']) {
          this.next_page = page + 1;
        }
        if (params['page'] == 2) {
          this.next_page = 1;
        }
        
      }

      //devolver listado de usuarios
      this.getColors(this.page);
    });
  }

  getColors(page = null) {
    this._dataService.getColors(page).subscribe(
      (response) => {
        if (response) {
          this.ad = response.ad;
          this.data = response.data;
          this.page = response.page;
          this.per_page = response.per_page;
          this.total = response.total;
          this.total_pages = response.total_pages;
          console.log(this.total_pages)
        } else {
          console.log('Error en la carga de datos del servidor');
        }
        console.log(response);
      },
      (error) => {
        console.log(<any>error);
      }
    );
  }

  myFunction(color) {
    let dummyContent = color;
    let dummy = $('<input>').val(dummyContent).appendTo('body').select();
    document.execCommand('copy');
    $(dummy).remove();
  }
}
