import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  public url: string;

  constructor(private _http: HttpClient) {
    this.url = 'https://reqres.in/api/colors/';
  }

  getColors(page = 1): Observable<any> {


    return this._http.get(this.url + '?page=' + page);
  }
}
