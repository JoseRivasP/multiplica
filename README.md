# MultiplicaTest

Este proyecto es generado bajo angular en su ultima version version 9.1.3.

## Frameworks utilizados

En este projecto se utilizo la ultima version de Bootstrap hasta el momento la 4.4.1 y de JQuery la 3.4.1 solo debes ejecutar el comando `npm install` y seran instalados automaticamente.


## Servidor local de desarrollo

Para generar un servidor local de desarrollo utilizas el comando `ng serve` o `npm start` y esto levantara el entorno en la url `http://localhost:4200/`. 

## Generar compilado de despliegue
para generar un compilado para ser desplegado lo puedes hacer con el comando `ng build` lo cual generará una carpeta `dist/` en donde estara el HTML, CSS y Javascript que debes utilizar para el despliegue en tu servidor.

## Deje la aplicacion en un repositorio publico dentro de mi cuenta de gitlab (tengo mas proyectos pero estan en privado)
https://gitlab.com/JoseRivasP/multiplica

## Desplegue mi aplicacion en la siguiente url en el servidor Netlify
https://pedantic-kilby-dacec6.netlify.app/

